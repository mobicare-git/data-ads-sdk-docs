# Requisitos

- Mínima versão de Javascript suportada `es5`

## Módulos

- Rewarded Video


### 1. Importação da SDK 

#### Utilizando a tag `<script>`
Você receberá três scripts contendo as dependências `sdk-core`, `sdk-ads-core` e `sdk-ads-rv`. Juntos eles ocupam um espaço de 120KB. Inclua os arquivos no seu projeto e inclua as tags script abaixo:
```html
<script src="./sdk-core.js">
<script src="./sdk-ads-core.js">
<script src="./sdk-ads-rv.js">
```

#### Importando via módulo
Você receberá um zip contendo as dependências `sdk-core`, `sdk-ads-core` e `sdk-ads-rv`. Juntos eles ocupam um espaço de 211KB. Realize a descompressão do arquivo e em seguida adicione ao seu package.json as dependências abaixo:

```shell
mkdir sdk
unzip dataads-sdk-js-v1.0.0.zip -d sdk/
```

```javascript
{
  "name": "sample-app",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "@akross-dataads-sdk/ads-core": "file:./sdk/sdk-ads-core",
    "@akross-dataads-sdk/ads-rv": "file:./sdk/sdk-ads-rv",
    "@akross-dataads-sdk/core": "file:./sdk/sdk-core",
  }
}
```

### 2. Inicialização da SDK
Para carregar o SDK JS é necessária a importação do módulo Core e dos módulos dos Ads. 
- Módulo Core
```javascript
import { AACampaign, AAMedia, ManagerCore } from "@akross-dataads-sdk/core"; 
import { IManagerCore } from "@akross-dataads-sdk/core/dist/IManagerCore"; 

```

- Módulo ADS
```javascript
import { ManagerAdsCore } from "@akross-dataads-sdk/ads-core"; 
import { ManagerAdsRV } from "@akross-dataads-sdk/ads-rv";
```
> O módulo `@akross-dataads-sdk/ads-rv` tem o objetivo de exibir conteúdo programático, logo, uma das dependências externas que ele possui é a biblioteca do GPT (Google Publisher Tag).

Para inicialização do SDK segue script abaixo:
```javascript
const manager: IManagerCore = new ManagerCore();
await manager.initialize(APPLICATION_KEY, ARTEMIS_BASE_URL, true);
```
Onde o `APPLICATION_KEY`  é a chave de identificação da sua aplicação e `ARTEMIS_BASE_URL` é a URL do seu domínio configurado no Data Ads disponibilizado para o carregamento de campanha. O último parâmetro serve para habilitar/desabilitar o modo DEBUG da SDK. 


### 3. Registro do módulo de publicidade
Para cada módulo de publicidade que deseja utilizar, é necessário registrar no gerenciador de publicidade o módulo importado

```javascript
const managerAds = new ManagerAdsCore(manager);
managerAds.registerMediaModule(ManagerAdsRV.register());
```

### 4. Custom Headers (opcional)

Em casos em que seja necessário enriquecer a chamada com outros headers, basta executar a chamada abaixo:

```javascript
await manager.setCustomHeaders({
  authorization: TOKEN_HEADER,
});
```

### 5. Identificação de usuário

Antes de iniciar o carregamento da campanha, é necessário informar o identificador do usuário ao SDK, de forma que, em cada carregamento de campanha:
```javascript
await manager.setUserId(USER_ID);
```


### 6. Carregamento de publicidade

Após o sdk ser inicializado, será possível carregar as campanhas de publicidade. Para isso, será necessário 4 informações:
- Identificação da Zona (`ZONE_UUID`)
    - Id único passado pela equipe responsável pelo Artemis que irá identificar ao serviço de campanhas o destino da campanha a ser entregue.
- Quantidade de campanhas a serem retornadas (`100`)
    - Indica o máximo de campanhas a serem retornadas na chamada do SDK. Não existe paginação neste fluxo.
- Tipo de chave do usuário (`1`)
    - Identificador numérico que representa o tipo de chave que foi inicializada. Se durante a configuração da zona foi definido que mais de um tipo de chave foi informado, (ex.: msisdn e walletId, nesta ordem), o tipo de chave é a posição do array (que neste exemplo, para o msisdn, o userKeyType = 0 e para o walletId o userKeyType = 1).
- Informações de contexto do usuário (`contextInfo`)
    - É um objeto contendo informações dos usuários no momento em que a publicidade será carregada e que possa ser utilizado para segmentar o usuário. Exemplo: ddd, localização do usuário, bateria, temperatura atual, etc. 

```javascript
const contextInfo: { [key: string]: string } = {
    ddd: "123123",
};
//Requisição de campanhas
const result = await managerAds.getCampaign(
    ZONE_UUID,
    100,
    1,
    contextInfo
);
```


### 7. Lançamento de evento de impressão
No momento em que são carregadas as campanhas, pode não ser o momento em que elas são exibidas, logo, só chamamos o envio de eventos de impressão quando as campanhas são renderizadas na tela. 

```javascript
  await managerAds.sendCampaignsImpressionEvent(
    campaignResponse.requestId,
    campaignResponse.campaigns
  );
```

### 8. Renderização de mídia

Para abrir uma mídia, basta chamar o método abaixo:

```javascript
const result = await managerAds.openMedia(media);
```

A função `managerAds.openMedia(media)` executa a mídia especificada, seguindo todos os passos previstos com base no tipo da mídia.

@param media — O objeto AAMedia que representa a mídia a ser executada.

@returns
Uma Promise que resolve com o resultado da execução da mídia (AAAdWatchResult), contendo:

- media: A própria mídia que foi executada.
- error: Informações sobre erros que ocorreram durante a execução, caso existam.
- result: Um enum que indica o código de retorno da execução, podendo ser:
  - REWARDED = 11
  - REWARDED_REDIRECTED = 12
  - MEDIA_WATCHED = 13
  - SKIPPED = 20
  - ERROR = 21
  - CONNECTION_ERROR = 22
  - NO_FILL = 23
  - CANCELED = 24
  - POLL_WRONG_ANSWER = 25

Este método é fundamental para a execução de mídias, garantindo que todos os passos específicos ao tipo de mídia sejam seguidos e que o resultado seja retornado de forma estruturada.

Para cada tipo de mídia, `managerAds.openMedia(media)` terá um comportamento de acordo com o seu tipo:
- **programatica**: abrirá um modal com a SDK do `$.config.type` configurado, exemplo, com a SDK do Google Ads caso `$.config.type=google-admob-rv`
- **external_redirect**: Um modal será aberto, sendo exibido os textos principal e secundário e um botão com o link configurado. Ao clicar no modal, o usuário será redirecionado para a página externa.
- **data_search**: um modal é aberto exibindo a pesquisa. Se `$.content.dataSearch.layout=landing-page`, todas as perguntas são exibidas em uma única página. Caso contrário, será exibida uma pergunta por página.
- **image**: um modal é aberto exibindo a imagem. Se um botão for configurado, também será exibido, logo abaixo da imagem.
- **vast**: Um modal é aberto com um player de vídeo
- **vpaid**: Um modal é aberto com um player de vídeo
- **vastSurvey**: Um modal é aberto com um player de vídeo. Após a finalização do vídeo, é exibida uma pesquisa, no mesmo formato da mídia `data_search`
- **vpaidSurvey**: Um modal é aberto com um player de vídeo. Após a finalização do vídeo, é exibida uma pesquisa, no mesmo formato da mídia `data_search`
