# Requisitos

- Certificado de push do iOS
- Conta de serviço do Firebase com acesso ao FCM (JSON) ou URL do SNS na AWS
- Projeto com iOS target 11.0

# Módulos

### Core

Esse módulo contém tudo que for comum a todos os módulos como as Utils e extensions, possuindo também o controle da camada HTTP e o envio de Reports. É o único módulo obrigatório, contendo o processo de inicialização da SDK, identificação do usuário, envio de trackers e request de permissões.

### Notification
É responsável por parsear e exibir as campanhas de push notification no formato correto. Também é responsável por exibir notificações locais de forma "passiva" ao buscar esse tipo de campanha no adserver, dado alguns triggers, cuidando também do registro e desregistro no servidor de push.

### Ads Core
 É uma dependência obrigatória para todos os módulos de ads. Possui todo código comum aos módulos com requests para obtenção de campanhas, adicionando os dados do usuário e possíveis dados de contexto do device. O cliente irá lidar com os objetos retornados no sucesso desse request.

### Ads Banner
 Esse módulo contém uma View customizada que executa a chamada de getCampaign e ao ser carregada, imprime a primeira mídia do tipo imagem encontrada e faz os envios dos eventos de impressão e click.

### Ads RV (Rewarded Video)

 Contém as SDK's terceiras de rewarded vídeo e possui a SDK do Google Ads. Esse módulo contém um ViewController responsável por montar a tela com os RV's configurados para cada mídia e retornando um resultado do que ocorreu para o app hospedeiro.

### Ads Poll (Pesquisa)

 Contém a tela referente à mídias do tipo "pesquisa", o trigger dessa tela é um método .onClick no objeto de mídia e o resultado da operação é retornado para client via result. Essa tela pode ser montada de formas diferentes de acordo com uma configuração vinda da campanha: Uma página para cada pergunta ou uma página única para cada pesquisa.

# 1. Importação da SDK Ads

Adicione os arquivos artemis_notification.framework e artemis_core.framework no projeto. Marque a opção "copy items if needed"

### Controle de URL e Keys

As URL's e chaves de acesso deverão ser adicionadas ao arquivo: **ArtemisAdsConfig.plist**, exemplo:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>API_BASE_URL</key>
	<string>https://api.example.com.br/</string>
	<key>API_HOMOLOG_URL</key>
	<string>https://api.homolog.com.brt</string>
</dict>
</plist>

```

### Configuração de cores
A cor padrão para os botões incluidos na SDK pode ser gerenciada através do arquivo: **Assets.xcassets**. Dentro do arquivo já existe um ColorSet criado chamado **AccentColor**, que possui por padrão a cor #002198, mas que pode ser alterado por qualquer outro Hexadecimal ou floating point.



# 2. Inicialização da SDK
Adicione o seguinte snippet na classe **AppDelegate** no método **didFinishLaunchingWithOptions**.

```swift
AAManager.sharedInstance.initialize(appKey: "your-app-key")
```

# 3. Requisitar Permissões (opcional)
Os relatórios do SDK podem utilizar a localização do usuário para melhorar a experiência de entrega de ads.
Para isso é necessário requisitar a permissão de acesso à localização usando o snippet abaixo:

```swift
AAManager.sharedInstance.requestLocationPermission()
```

No iOS temos que adicionar no projeto nativo em info.plist as strings com os textos explicativos sobre o pedido de permissão de acesso a localização nas chaves NSLocationAlwaysAndWhenInUseUsageDescription e NSLocationWhenInUseUsageDescription.

# 4. Identificando o usuário

Assim que o usuário puder ser identificado (após o fluxo de login por exemplo), deve ser chamado o seguinte método:

```swift
AAManager.sharedInstance.setUserId(userId: 'user-id')
```

# 5. Banner

O banner pode ser exibido em um componente customizado chamado ArtemisAdsView. Esse componente ocupa a largura definida no layout e o banner carregado tem sua altura alterada para manter a proporção.

Esse componente pode ser carregado diretamente na viewController, como por exemplo nesta função:


```swift
    @IBAction func requestBanner(_ sender: Any) {
    //criando uma variavel e instanciando a view em que é construido o banner
        let bannerView = ArtemisAdsView()
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
    //Configurando Constraint do banner (Ajustes de posicionamento)
        NSLayoutConstraint.activate([
            bannerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            bannerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            bannerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
    //setando um ZoneId para realizar a request das campanhas
        bannerView.setZoneId("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx")
    // listener de sucesso
        bannerView.successListener = self
    }
```

Existem alguns listeners com callbacks para o carregamento dos banners. Esses listeners retornam um valor chamado analyticsTag que é cadastrado no dashboard para eventuais eventos de analytics do hospedeiro, possuimos também um listener para mapear possíveis erros.

```swift
   public protocol ArtemisAdsViewClickDelegate: AnyObject {
    func onClick(analyticsTag: String?)
	}

    public protocol ArtemisAdsViewSuccessDelegate: AnyObject {
        func onSuccess(analyticsTag: String?)
    }

    public protocol ArtemisAdsViewErrorDelegate: AnyObject {
        func onError()
    }
```

Exemplo de uso:
```swift
  extension ViewController: ArtemisAdsViewSuccessDelegate{
    func onSuccess(analyticsTag: String?) {
        print("Image loaded successfully with analytics tag: \(analyticsTag)")
    }
}
```

# 6. Notificações

## Configurando o projeto iOS
### Capabilities

No XCode, caso seja necessário adicione a capability de Push Notification no projeto.
Essa configuração também deverá ser feita no console da Apple

![](readme-assets/1.png?raw=true)

### App Groups

Ainda em capabilities, clique no botão + para adicionar um novo app grouo.

![](readme-assets/2.png?raw=true)

O id deve estar no formato **group.**appBundleId**.aa** onde appBundleId é o bundle Id do aplicativo hospedeiro.

![](readme-assets/3.png?raw=true)

### Service Extensions

No XCode, deverá ser adicionado um novo target

![](readme-assets/4.png?raw=true)

O target deverá ser do tipo "NotificationServiceExtension"

![](readme-assets/5.png?raw=true)

O nome deve ser **ArtemisAdsNotificationService**.

![](readme-assets/6.png?raw=true)

Ao ser perguntando se deseja ativar o novo target, clique em "Activate".

![](readme-assets/7.png?raw=true)

A classe gerada NotificationService.swift deverá ter o seguinte código:

```swift
import UserNotifications
import UIKit
import artemis_notification

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    var receivedRequest: UNNotificationRequest!

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        self.receivedRequest = request
        
        if let bestAttemptContent = bestAttemptContent {
            AANotificationManager.shared.didReceiveNotificationExtensionRequest(request: request, bestAttemptContent: bestAttemptContent, contentHandler: contentHandler)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
```

No target ArtemisNotificationService Extension, adicione o capability de app groups

![](readme-assets/9.png?raw=true)

O grupo deverá ser o mesmo da extension do app

![](readme-assets/10.png?raw=true)

Após efetuar as configurações, adicione os frameworks artemis_notification.framework e artemis_core.framework no target do Notification Service Extension na guia General >> Frameworks and libraries

## Registrando o app para receber push notifications

Após o usuário ser identificado, assim que for possível obter o token do FCM, deverá ser feito o registro do usuário no serviço de push. (isso normalmente é feito após o login). 

```swift
AANotificationManager.shared.registerForPushNotification(deviceId: uuid, userToken: deviceToken, fcmToken: fcmToken)
```

Onde uuid é o id do device, deviceToken é o token da APNS e fcmToken o token do FCM

## Desregistrando o app para receber push notifications

Também deve ser requisitado que o servidor pare de enviar push's para o device caso o usuário deslogue da aplicação:

```swift
AANotificationManager.shared.unregisterForPushNotification()
```

## Processando pushs recebidos

Na classe **AppDelegate**, no método didReceive do delegate do UNUserNotificationCenter deverá ser adicionado o seguinte código:

```swift
func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        let responseIdentifier = response.actionIdentifier
        AANotificationManager.shared.handlePushNotification(actionId: responseIdentifier, userInfo: userInfo)
    }
```

