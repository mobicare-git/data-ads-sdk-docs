#Requisitos

- Mínima versão de Android suportada 5.0
- Conta de serviço do Firebase com acesso ao FCM (JSON) ou URL do SNS na AWS
- Firebase messaging configurado no projeto (https://firebase.google.com/docs/cloud-messaging/android/client)
#Módulos

- Push notification
- Banner

#1. Importação da SDK Oi Ads

No build.gradle (project level) adicionar o repositório da Mobicare

As URL's e chaves de acesso ao repositório Mobicare deverão ser passados na contratação


```groovy
allprojects {
    repositories {
        maven {
            url "${ARTIFACTORY_CONTEXT_URL}/${ARTIFACTORY_REPOSITORY_KEY_PUBLISH}"
            credentials {
                username = ARTIFACTORY_USER
                password = ARTIFACTORY_PASSWORD
            }
            content {
                includeModule("br.com.mobicare", "artemis-notification")
                includeModule("br.com.mobicare", "artemis-core")
                includeModule("br.com.mobicare", "artemis-ads-banner")
                includeModule("br.com.mobicare", "artemis-ads-core")
                includeModule("br.com.mobicare", "artemis-ads-rv")
                includeModule("br.com.mobicare", "artemis-ads-poll")
            }
        }
    }
}
```

No build.gradle (app level) importar as dependências dos módulos utilizados

```groovy
def artemisSdkVersion = "2.0.1"
implementation(group: 'br.com.mobicare', name: 'artemis-notification', version: "$artemisSdkVersion", ext: 'aar'){
    transitive = true
}
implementation(group: 'br.com.mobicare', name: 'artemis-ads-banner', version: "$artemisSdkVersion", ext: 'aar'){
    transitive = true
}
```

#2. Inicialização da SDK
Na activity inicial do projeto adicione o seguinte snippet

```java
AAManager.init(context, "your-app-key", initCompleteCallback = {
    //necessário caso esteja usando módulo de notificações
    AANotificationManager.createNotificationChannel(context)
})
```

#3. Requisitar Permissões (opcional)
Os relatórios do SDK podem utilizar a localização do usuário para melhorar a experiência de entrega de ads.
Para isso é necessário requisitar a permissão de acesso à localização usando o snippet abaixo:

```java
AAManager.requestPermission(context)
```

#4. Identificando o usuário

Assim que o usuário puder ser identificado (após o fluxo de login por exemplo), deve ser chamado o seguinte método:

```java
AAManager.setUserId(context, "userId")
```

Esse passo é obrigatório para os fluxos de push e publicidade

#5. Banner

O banner pode ser exibido em um componente customizado chamado ArtemisAdsView.
Esse componente ocupa a largura definida no layout e o banner carregado tem sua altura alterada para manter a proporção.

Esse componente pode ser carregado via xml:

``` XML
<br.com.mobicare.aa.ads.modules.widget.ArtemisAdsView
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:zoneId="<zoneId>"/>
```

ou diretamente na activity/fragment

```java
val artemisAdsBanner = findViewById<ArtemisAdsView>(R.id.main_activity_home_artemis_ads)
artemisAdsBanner.loadZone("<zoneId>")
```

Existem alguns listeners com callbacks para o carregamento dos banners. Esses listeners retornam um valor chamado analyticsTag que é cadastrado no dashboard para eventuais eventos de analytics do hospedeiro

```java
//chamado quando o banner é clicado
artemisAdsBanner.setClickListener(object : ArtemisAdsView.ArtemisAdsViewClickListener {
    override fun onClick(analyticsTag: String?) {
        Log.v("ArtemisAdsSample", "Banner clicked! $analyticsTag")
    }
})

//chamado quando o banner é carregado
artemisAdsBanner.setSuccessListener(object : ArtemisAdsView.ArtemisAdsViewSuccessListener {
    override fun onSuccess(analyticsTag: String?) {
        Log.v("ArtemisAdsSample", "Image loaded successfully! $analyticsTag")
    }
})
```


#6. Notificações

##Registrando o app para receber push notifications

Após o usuário ser identificado através do método setUserId, assim que for possível obter o token do FCM, deverá ser feito o registro do usuário no serviço de push. (isso normalmente é feito após o login)

```java
AANotificationManager.registerForPushNotification(context, "fcmToken")
```

##Desregistrando o app para receber push notifications

Também deve ser requisitado que o servidor pare de enviar push's para o device caso o usuário deslogue da aplicação:

```java
AANotificationManager.unregisterForPushNotification(context, "token")
```

Deverá ser adicionada no projeto uma classe que extende à FirebaseMessagingService. Caso já exista alguma no projeto, deve ser adicionado os snippets a seguir nos métodos correspondentes:

```java
class NotificationListenerService : FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val map = message.data
        if(AANotificationManager.isAANotification(applicationContext, map)) {
            AANotificationManager.showNotification(applicationContext, map, <drawable exibido na status bar>)
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
		AANotificationManager.registerForPushNotification(context, token)
    }
}
```
