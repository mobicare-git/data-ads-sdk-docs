# Load Campaigns

Abaixo segue a descrição de cada atributo do payload das campanhas retornados pelo método  `managerAds.getCampaign(...)`. Versão utilizada pelo SDK: `v4`.

## Payload principal

| Atributo          | Tipo | Descrição |
|-------------------|------|-----------------------------|
| $.requestId       |`string`| Id da requisição. É um id único por request |
| $.userId          |`string`| Id do usuário da solicitação das campanhas |
| $.zone            |`object`| Informações da zona |
| $.zone.uuid       |`string`| Identificador da zona (no qual foi utilizado para carregar as campanhas) |
| $zone.name        |`string`| Nome único da zona |
| $zone.description |`string`| Descrição da zona |
| $zone.extra       |`object`| Configuração da zona |
| $.ka              |`number`| Keep-alive. Representa quanto tempo o $.requestId é válido para que os eventos possam ser registrados. Caso contrário, são registrados como fraude.     |
| $.campaigns       |`object`| Lista de campanhas ativas nesta zona |
| $.campaigns[i].campaignUuid           |`string`| Identificador da campanha |
| $.campaigns[i].campaignName           |`string`| Nome da campanha cadastrado na plataforma|
| $.campaigns[i].campaignStartDate      |`timestamp`| Data de início da campanha|
| $.campaigns[i].campaignEndDate        |`timestamp`| Data de fim da campanha|
| $.campaigns[i].trackingId             |`string`| Mesmo que $.requestId|
| $.campaigns[i].priority               |`number`| Prioridade da campanha|
| $.campaigns[i].carrotPackageEnabled   |`boolean`| Indica se a campanha possui pacote cenoura habilitado.  |
| $.campaigns[i].mainData               |`object`| Criativo da campanha a ser renderizado.  |
| $.campaigns[i].mainData.type          |`string`| Tipo do conteúdo, se é interno ou externo. |
| $.campaigns[i].mainData.layout        |`object`| Possui os atributos de apresentação da campanha  |
| $.campaigns[i].mainData.success       |`object`| Tela de sucesso da campanha. Pode ser uma tela interna ou externa  |
| $.campaigns[i].mainData.media         |`array[object]`| Lista de mídias cadastradas, que podem ser dos tipos `image`, `programmatic`, `data_search`, `vast`, `vpaid`, `vastSurvey`, `vpaidSurvey`, `external_redirect` |
| $.campaigns[i].benefitOffers          |`array[object]`| Lista de benefícios que serão concedidos nesta campanha  |


### Layout
Descrição dos atributos de campanha do objeto `$.campaigns[i].mainData.layout`
| Atributo                  | Tipo   | Descrição |
|---------------------------|--------|-----------------------------|
| $.primaryColor            |`string`| Cor primária a ser utilizada na campanha, no formato RGB. Ex. `#FFFFFF`  |
| $.primaryInformation      |`string`| Texto principal da campanha |
| $.secondaryColor          |`string`| Cor secundária a ser utilizada na campanha, no formato RGB. Ex. `#FFFFFF`  |
| $.secondaryInformation    |`string`| Texto secundário da campanha |
| $.header                  |`object`| Contém informações do cabeçalho da campanha  |
| $.header.image            |`string`| Url do banner cadastrado na plataforma  |
| $.title                   |`string`| Título de apreentação da campanha  |
| $.icon                    |`string`| Url do ícone da campanha  |


### Success
Descrição dos atributos de campanha do objeto `$.campaigns[i].mainData.success`
| Atributo                      | Tipo   | Descrição |
|-------------------------------|--------|-----------------------------|
| $.type                        |`string`| Pode ser `internal` ou `external`  |
| $.title                       |`string`| Título da tela de sucesso. Preenchido somente quando type `internal`. |
| $.description                 |`string`| Descrição da tela de sucess.  Preenchido somente quando type `internal`  |
| $.url                         |`string`| URL no qual o usuário deverá ser redirecionado quando completar a campanha.  Preenchido somente quando type `external` |
| $.autoRedirectUrl             |`object`| Objeto que informa que o usuário deverá ser redirecionado após X segundos da tela de sucesso ser exibida. |
| $.autoRedirectUrl.url         |`string`| URL do redirecionamento |
| $.autoRedirectUrl.waitSeconds |`number`| Tempo, em segundos, no qual a tela de sucesso deverá ser eibida antes do usuário ser redirecionado. |
| $.buttons                     |`array[object]`| Lista de botões para serem exibidos na tela de sucesso. |
| $.buttons[i].label            |`string`| Texto do botão. |
| $.buttons[i].action           |`object`| Objeto de ação do botão. |
| $.buttons[i].action.type      |`string`| Tipo de ação. Valor default: `link` |
| $.buttons[i].action.callback  |`string`| Ação a ser realizada. Normalmente é uma URL. |



### Benefícios
Objeto de benefício recebido em `$.campaigns[i].benefitOffers`

| Atributo          | Tipo | Descrição |
|-------------------|------|-----------------------------|
| $.uuid       |`string`| Identificador do benefício  |
| $.name       |`string`| Nome do benfício  |
| $.description|`string`| Descrição do benefício  |
| $.quantity   |`string`| Quantidade do benefício a ser concedido. Apesar de vir uma string, o valor sempre será numérico  |
| $.unit       |`string`| Unidade do benefício. Pode representar "moeda", "pontos", "voucher", etc.  |


### Mídias

Todos os objetos abaixo representam os tipos de mídias que virão dentro do objeto `$.campaigns[i].mainData.media[i]`.

#### Atributos recebidos em todas as mídias
| Atributo        | Tipo | Descrição |
|-----------------|------|-----------------------------|
| $.uuid          |`string`| Id da mídia |
| $.type          |`string`| Tipo da mídia. Influencia no formato do `$.content` que será carregado |
| $.title         |`string`| Título da mídia |
| $.content       |`object`| Propriedades que representam o tipo da mídia.  |
| $.qntReplicas   |`number`| Quantas vezes essa mídia foi replicada |
| $.properties    |`object`| Propriedades da mídia. É alterada conforme o tipo de mídia |
| $.viewed        |`boolean`| Se a mídia já foi visualizada pelo usuário `userId` |
| $.proxy         |`boolean`| Se o proxy está habilitado naquela mídia, ou seja, todas as requisições passam pelo adserver antes de serem encaminhadas para a URL original. |

#### Tipo de mídia: Programatica
| Atributo                  | Tipo | Descrição |
|---------------------------|------|-----------------------------|
| $.type                    |`string`| Valor: `programatica` |
| $.config                  |`object`| Objeto contendo a configuração do bloco de anúncio utilizado |
| $.config.type             |`string`| Provedor externo utilizado. Opções disponíveis: `google-admob-rv`, `intelifi`, `iron-source-rv`, `tapjoy-rv` |
| $.config.params           |`array[object]`| Lista de parâmetros configurados para o provedor externo. |
| $.config.params[i].key    |`string`| Nome do atributo |
| $.config.params[i].value  |`string`| Valor do atributo |
| $.fallbacks-config        |`array[object]`| lista de blocos de anúncios a serem utilizados em caso de no-fill na configuração de `$.config`. Objeto possui os mesmos atributos de `$.config`. |
| $.fallbacksNoFill         |`object`| É uma mídia que deve ser renderizada quando todos os blocos de anúncio configurados falham. Pode ser do tipo: `vast` ou `vpaid` |


#### Tipo de mídia: External Redirect
| Atributo          | Tipo | Descrição |
|-----------------------------|------|-----------------------------|
| $.type                      |`string`| Valor: `external_redirect` |
| $.content.infoPrimarty      |`string`|  Texto principal da mídia |
| $.content.infoSecondary     |`string`|  Texto secundário da mídia |
| $.content.actionButtonLabel |`string`|  Texto para ser incluído no botão |
| $.content.link              |`string`|  URL no qual redireciona o usuário ao site do parceiro |
| $.content.thumbnail         |`string`|  URL da imagem que compõe a exibição da mídia |


#### Tipo de mídia: Data Search
| Atributo                                 | Tipo | Descrição |
|------------------------------------------|---------|-----------------------------|
| $.type                                   |`string`| Valor: `data_search` |
| $.content.dataSearch.rightAnswerRequired |`boolean`|  Indica se as perguntas devem ser respondidas obrigatoriamente |
| $.content.dataSearch.termPosition        |`string`|  Indica quando os "termos e condições" devem ser exibidos, se antes da pesquisa (`before`) ou depois (`after`) da pesquisa. Se vier uma string vazia ou nulo, não existe termo.|
| $.content.dataSearch.confirmationTerm    |`string`|  Texto do termo de confirmação no formato `HTML`.|
| $.content.dataSearch.layout              |`string`|  Indica o formato em que a pesquisa deverá ser renderizada. Se `step-page`, cada pergunta deverá ser exibida em uma página separada. Se `landing-page`, todas as perguntas são exibidas em uma única página. Se o atributo vier vazio, o valor padrão é `step-page` |
| $.content.dataSearch.questions           |`array[object]`| Lista de perguntas |


##### Tipo de mídia: Data Search - Questions
Todos os atributos abaixo representam as questões de uma mídia de pesquisa: `$.content.dataSearch.questions`.
| Atributo                | Tipo     | Descrição                   |
|-------------------------|----------|-----------------------------|
| $.uuid                  |`string`|  Identificador da pergunta |
| $.title                 |`string`|  Título da pergunta |
| $.type                  |`string`|  Tipo de pergunta. Pode ser: `check`, `radio`, `text`, `date`,`` |
| $.required              |`boolean`|  Indica se a pergunta é obrigatória |
| $.benefitOffers         |`array[object]`|  Lista de benefícios a ser concedido se o usuário acertar a pergunta. Se a pergunta não for obrigatória, o benefício é concedido.  |
| $.rightAnswerRequired   |`boolean`|  Indica se a pergunta deve ser respondida de forma correta. |
| $.options               |`array[object]`|  Lista de respostas disponíveis. Disponível somente quando `$.type=check` ou `$.type=radio` |
| $.options.uuid          |`string`|  Identificador da pergunta |
| $.options.description   |`string`|  Descrição da pergunta da pergunta |
| $.options.isAnswer      |`boolean`| Identifica se é a resposta correta I |
| $.options               |`array[object]`|  Lista de respostas, quando `$.type=check` ou `$.type=radio` |
| $.fieldType             |`string`|  Quando `$.type=text`, este atributo vem preenchido com um dos possíveis valores: `text`, `email`, `cpf`, `phone` ou `cep`.  |
| $.validateMax           |`boolean`|  Quando `$.fieldType=text`, este atributo indica se haverá validação de tamanho máximo do texto. |
| $.validateMin           |`boolean`|  Quando `$.fieldType=text`, este atributo indica se haverá validação de tamanho mínimo do texto. |
| $.max                   |`number`|  Quando `$.fieldType=text`, representa o máximo de caracteres que essa resposta pode possuir.  |
| $.min                   |`number`|  Quando `$.fieldType=text`, representa o mínimo de caracteres que essa resposta pode possuir.  |
| $.acceptHour            |`boolean`| Quando `$.type=date`, indica se deverá receber hora como resposta.  |
| $.dateFormat            |`number`|  Quando `$.type=date`, formato da data/hora. Aceita `DD/MM/YYYY`, `DD/MM/YYYY HH:MM`, `MM/DD/YYYY` ou `MM/DD/YYYY HH:MM`  |
| $.dateMaxType           |`string`|  Quando `$.type=date`, representa a validação da data máxima que poderá ser aceita. Valores possíveis: `no-limit`, `now`, `yesterday`, `tomorrow`, `fixed`   |
| $.dateMaxType           |`string`|  Quando `$.type=date`, representa a validação da data mínima que poderá ser aceita. Valores possíveis: `no-limit`, `now`, `yesterday`, `tomorrow`, `fixed`   |
| $.dateMax               |`date`|  Quando `$.dateMaxType=fixed`, representa a data máxima essa resposta pode possuir.  |
| $.dateMin               |`date`|  Quando `$.dateMaxType=fixed`, representa a data mínima essa resposta pode possuir.  |



#### Media Type: Image
| Atributo          | Tipo | Descrição |
|-----------------------------|------|-----------------------------|
| $.type                             |`string`| Valor: `image` |
| $.content.url                      |`string`|  URL da imagem |
| $.externalUrl.url                  |`string`|  URL externa no qual o usuário pode ser redirecionado ao clicar na imagem  |
| $.properties                       |`object`|  Propriedades da mídia |
| $.properties.resolutions           |`array[object]`|  Lista com a resolução da imagem |
| $.properties.resolutions[i].id     |`string`|  ID da resolução |
| $.properties.resolutions[i].label  |`string`|  Descrição da resolução |
| $.properties.resolutions[i].width  |`number`|  Largura da imagem |
| $.properties.resolutions[i].height |`number`|  Altura da imagem |
| $.properties.resolutions[i].fixed  |`boolean`|  Se a imagem ficará com o tamanho fixo ou não.  |
| $.properties.analytics             |`array[object]`|  Lista dos eventos reportados na imagem |
| $.properties.analytics[i].key      |`string`|  Nome do evento |
| $.properties.analytics[i].label    |`string`|  Descrição do evento |



#### Media Type: Vast
| Atributo          | Tipo | Descrição |
|-----------------------------|------|-----------------------------|
| $.type                             |`string`| Valor: `vast` |
| $.content.url                      |`string`| URL contendo o XML Vast |
| $.externalUrl.url                  |`string`|  URL externa no qual o usuário pode ser redirecionado ao clicar no vídeo  |
| $.modeVideo                        |`boolean`| Significa que o vídeo foi carregado na plataforma e a mesma gerou um XML Vast |
| $.originalContent                  |`string`| URL do vídeo original, quando o mesmo foi carregado via plataforma. |


#### Media Type: Vpaid
| $.type                             |`string`| Valor: `vpaid` |
| $.content.url                      |`string`| URL contendo o XML Vpaid |

#### Media Type: Vast Survey
| $.type                             |`string`| Valor: `vastSurvey` |
| $.content.url                      |`string`| URL contendo o XML Vast |
| $.content.dataSearch               |`object`| Objeto contendo o formulário para que o usuário responda à pesquisa. Olhar `Media Type: Data Search` para maiores detalhes. |

#### Media Type: Vpaid Survey
| $.type                             |`string`| Valor: `vpaidSurvey` |
| $.content.url                      |`string`| URL contendo o XML Vpaid |
| $.content.dataSearch               |`object`| Objeto contendo o formulário para que o usuário responda à pesquisa. Olhar `Media Type: Data Search` para maiores detalhes. |

