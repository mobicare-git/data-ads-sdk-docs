#Requisitos

- Certificado de push do iOS
- Conta de serviço do Firebase com acesso ao FCM (JSON)
- Implementação do módulo capacitor/PushNotifications pelo app

#1. Importação do Oi Ads Plugin

Exportar conteúdo do plugin localmente e executar o npm install

```
npm install mcare-artemis-ads-sdk-ionic
npx cap sync
```

#2. Inicialização do Plugin
Adicione o seguinte snippet na classe **src/app/app.component.ts**.

```
MCareAdsManager.initSdk({appId: 'your-app-key'});
```

#3. Requisitar Permissões (opcional)
Os relatórios do SDK podem utilizar a localização do usuário para melhorar a experiência de entrega de ads.
Para isso é necessário requisitar a permissão de acesso à localização usando o snippet abaixo:

```
MCareAdsManager.requestPermission()
```

No iOS temos que adicionar no projeto nativo em info.plist as strings com os textos explicativos sobre o pedido de permissão de acesso a localização nas chaves NSLocationAlwaysAndWhenInUseUsageDescription e NSLocationWhenInUseUsageDescription.

#4. Identificando o usuário

Assim que o usuário puder ser identificado (após o fluxo de login por exemplo), deve ser chamado o seguinte método:

```
MCareAdsManager.setUserId({userId: 'user-id'});
```

#5. Banner

O banner será exibido em um componente tipo ```<ion-img>```.
Deverão ser incluídos os seguintes snippets para carregamento e tratamentos de eventos dos banners:

.html
```
<ion-img [src]="mcareAdsBannerData.bannerUrl" (click)="mcareAdsBannerClick()" (ionImgDidLoad)="mcareAdsBannerDidLoad()"></ion-img>
```

.ts

```
export class HomePage implements OnInit {
	private mcareAdsBannerData;
    
    .
    .
    .
    
    mcareAdsBannerClick() {
        MCareAdsManager.bannerClicked({
            clickUrl: this.mcareAdsBannerData.successUrl,
            campaignId: this.mcareAdsBannerData.campaignId,
            userKeyType: 2
        });
    }

    mcareAdsBannerDidLoad() {
        MCareAdsManager.sendImpressionReport({campaignId: this.mcareAdsBannerData.campaignId, userKeyType: 2});
    }
    
    loadBanner(){
        MCareAdsManager.getBannerData({
          zoneId: 'f8453a6a-d643-47a3-a811-1487df531a77',
          userKeyType: 2,
        }).then(r => {
          this.mcareAdsBannerData = r;
        });
	}
}

```

#6. Notificações

##Configurando o projeto iOS
###Capabilities

No XCode, caso seja necessário adicione a capability de Push Notification no projeto.
Essa configuração também deverá ser feita no console da Apple

![](readme-assets/1.png?raw=true)

###App Groups

Ainda em capabilities, clique no botão + para adicionar um novo app grouo.

![](readme-assets/2.png?raw=true)

O id deve estar no formato **group.**appBundleId**.aa** onde appBundleId é o bundle Id do aplicativo hospedeiro.

![](readme-assets/3.png?raw=true)

###Service Extensions

No XCode, deverá ser adicionado um novo target

![](readme-assets/4.png?raw=true)

O target deverá ser do tipo "NotificationServiceExtension"

![](readme-assets/5.png?raw=true)

O nome deve ser **ArtemisAdsNotificationService**.

![](readme-assets/6.png?raw=true)

Ao ser perguntando se deseja ativar o novo target, clique em "Activate".

![](readme-assets/7.png?raw=true)

Adicione o pod McareArtemisAdsSdkIonic como pod para o target da extension (o path pode variar):

```
target "ArtemisAdsNotificationService" do
  pod 'McareArtemisAdsSdkIonic', :path => '../../mcare-artemis-ads-sdk-ionic'
end

.
.
.

# adicionar no fim do arquivo
post_install do |installer|
    installer.pods_project.build_configurations.each do |config|installer.pods_project.targets.each do |target|
          target.build_configurations.each do |config|
            config.build_settings['APPLICATION_EXTENSION_API_ONLY'] = 'No'
          end
        end
    end
end
```

A classe gerada NotificationService.swift deverá ter o seguinte código:

```
import UserNotifications
import UIKit
import artemis_notification

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?
    var receivedRequest: UNNotificationRequest!

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        self.receivedRequest = request
        
        if let bestAttemptContent = bestAttemptContent {
            AANotificationManager.shared.didReceiveNotificationExtensionRequest(request: request, bestAttemptContent: bestAttemptContent, contentHandler: contentHandler)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
}
```

No target ArtemisNotificationService Extension, adicione o capability de app groups

![](readme-assets/9.png?raw=true)

O target deverá ser o mesmo da extension do app

![](readme-assets/9.png?raw=true)

Após efetuar as configurações, rode o comando 

```
npx cap sync
```

##Registrando o app para receber push notifications

Após o usuário ser identificado, assim que for possível obter o token do FCM, deverá ser feito o registro do usuário no serviço de push. (isso normalmente é feito após o login)

```
MCareAdsManager.registerForPushNotification({token: token.value, fcmToken: token.value});
```

##Desregistrando o app para receber push notifications

Também deve ser requisitado que o servidor pare de enviar push's para o device caso o usuário deslogue da aplicação:

```
MCareAdsManager.unregisterForPushNotification({token: token.value});
```

No listener notificationActionPerformed deverá ser adicionado o seguinte snippet:

```
const notificationInfo = {
  actionId: notification.actionId,
  notification: notification.notification.data.notification,
  type: notification.notification.data.type,
};

await MCareAdsManager.handleNotificationAction(notificationInfo);
```

No listener pushNotificationReceived deverá ser adicionado o seguinte snippet:

```
const options = {
  notification: notification.data.notification,
  trackingId: notification.data.TRAKING_ID,
  type: notification.data.type,
  appId: notification.data.ngtAppId,
  message: notification.data.alert,
};

MCareAdsManager.handleNotification(options);
```


##Configurando o projeto android

No arquivo .gradle (app) do projeto android, adicionar em repositories.flatDirs a seguinte linha

```
dirs '../../node_modules/mcare-artemis-ads-sdk-ionic/android/libs', 'libs'
```
