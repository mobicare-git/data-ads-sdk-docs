# Documentação do SDK

Este repositório contém a documentação de uso do SDK Data Ads nas seguintes linguagens/plataformas: `Android`, `Ionic + Capacitor`, `iOS` e `JavaScript`.

No diretório `Campaigns`, você encontrará detalhes sobre os dados retornados ao realizar a chamada para obtenção das campanhas.

